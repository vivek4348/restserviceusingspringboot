//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.06.29 at 12:46:35 PM EDT 
//


package com.exostar.icert.cam.user.userdeleterequesttype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.exostar.icert.cam.common.MessageHeaderType;


/**
 * <p>Java class for UserDeleteRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserDeleteRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://cam.boeing.com/common/model/MessageHeaderType}MessageHeaderType"/>
 *         &lt;element name="Body">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IFSUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDeleteRequestType", namespace = "http://cam.boeing.com/user/model/UserDeleteRequestType", propOrder = {
    "header",
    "body"
})
public class UserDeleteRequestType {

    // @XmlElement(name = "Header", namespace = "http://cam.boeing.com/user/model/UserDeleteRequestType", required = true)
    protected MessageHeaderType header;
    // @XmlElement(name = "Body", namespace = "http://cam.boeing.com/user/model/UserDeleteRequestType", required = true)
    protected UserDeleteRequestType.Body body;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link MessageHeaderType }
     *     
     */
    public MessageHeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageHeaderType }
     *     
     */
    public void setHeader(MessageHeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link UserDeleteRequestType.Body }
     *     
     */
    public UserDeleteRequestType.Body getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDeleteRequestType.Body }
     *     
     */
    public void setBody(UserDeleteRequestType.Body value) {
        this.body = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IFSUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ifsUserId"
    })
    public static class Body {

        // @XmlElement(name = "IFSUserId", namespace = "http://cam.boeing.com/user/model/UserDeleteRequestType", required = true)
        protected String ifsUserId;

        /**
         * Gets the value of the ifsUserId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIFSUserId() {
            return ifsUserId;
        }

        /**
         * Sets the value of the ifsUserId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIFSUserId(String value) {
            this.ifsUserId = value;
        }

    }

}
