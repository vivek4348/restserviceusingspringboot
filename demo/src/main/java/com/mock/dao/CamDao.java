package com.mock.dao;

public interface CamDao {
	
	public String getAllAuthorizationsOfServiceProviderForUser(String userId, String spId);

}
