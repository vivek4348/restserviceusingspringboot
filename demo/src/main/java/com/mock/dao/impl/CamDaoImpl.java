package com.mock.dao.impl;

import java.util.Date;

import org.springframework.stereotype.Repository;

import com.mock.dao.CamDao;

@Repository
public class CamDaoImpl implements CamDao {

	@Override
	public String getAllAuthorizationsOfServiceProviderForUser(String userId, String spId) {
		return "DAO - Some Encrypted String Corresponding to User: " + userId + " And SP: " + spId + " Date: " + new Date(); 
	}

}
