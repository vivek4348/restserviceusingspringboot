package com.mock.facade;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exostar.icert.cam.company.companycreateupdaterequesttype.CompanyCreateUpdateRequestType;
import com.exostar.icert.cam.company.companydeleterequesttype.CompanyDeleteRequestType;
import com.exostar.icert.cam.user.getuserauthorizationsrequesttype.GetUserAuthorizationsRequestType;
import com.exostar.icert.cam.user.getuserauthorizationsresponsetype.GetUserAuthorizationsResponseType;
import com.exostar.icert.cam.user.usercreateupdaterequesttype.UserCreateUpdateRequestType;
import com.exostar.icert.cam.user.userdeleterequesttype.UserDeleteRequestType;
import com.mock.service.CamService;

@RestController
@RequestMapping("rsapi")
public class CamFacadeservice {
	
	private static final Logger logger = LoggerFactory.getLogger(CamFacadeservice.class);
	
	 @Autowired
	 private CamService camService;
	
	@RequestMapping(value = "/hi", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String hiThere() {
		logger.debug("Hi Testing ");
		return "Version 2 Date: " + new Date() ;		
	}
	
	//http://localhost:8080/Camservice/getAllAuthorizationsOfServiceProviderForUser
	@RequestMapping(value = "/getAllAuthorizationsOfServiceProviderForUser", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetUserAuthorizationsResponseType getAllAuthorizationsOfServiceProviderForUser(@RequestBody GetUserAuthorizationsRequestType getUserAuthorizationsRequestType) {
		return camService.getAllAuthorizationsOfServiceProviderForUser(getUserAuthorizationsRequestType);		
	}
	
	@RequestMapping(value = "/createCompany", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Integer createCompany(@RequestBody CompanyCreateUpdateRequestType companyCreateUpdateRequestType) {
		return camService.createCompany(companyCreateUpdateRequestType);		
	}
	
	@RequestMapping(value = "/updateCompany", method = RequestMethod.PUT, produces = "application/json")
	public @ResponseBody Boolean updateCompany(@RequestBody CompanyCreateUpdateRequestType companyCreateUpdateRequestType) {
		return camService.updateCompany(companyCreateUpdateRequestType);		
	}
	
	@RequestMapping(value = "/deleteCompany", method = RequestMethod.DELETE, produces = "application/json")
	public @ResponseBody Boolean deleteCompany(@RequestBody CompanyDeleteRequestType companyDeleteRequestType) {
		return camService.deleteCompany(companyDeleteRequestType);		
	}
	
	@RequestMapping(value = "/createUser", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Integer createUser(@RequestBody UserCreateUpdateRequestType userCreateUpdateRequestType) {
		return camService.createUser(userCreateUpdateRequestType);		
	}
	
	@RequestMapping(value = "/updateUser", method = RequestMethod.PUT, produces = "application/json")
	public @ResponseBody Boolean updateUser(@RequestBody UserCreateUpdateRequestType userCreateUpdateRequestType) {
		return camService.updateUser(userCreateUpdateRequestType);		
	}
	
	@RequestMapping(value = "/deleteUser", method = RequestMethod.DELETE, produces = "application/json")
	public @ResponseBody Boolean deleteUser(@RequestBody UserDeleteRequestType userDeleteRequestType) {
		return camService.deleteUser(userDeleteRequestType);		
	}

}
