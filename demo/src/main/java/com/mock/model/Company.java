package com.mock.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "COMPANY")
public class Company {
	
	@Column(name = "COMPANY_NAME", unique = false, nullable = true)
	private String companyName = null;
	
	@Column(name = "IFS_ID", unique = false, nullable = true)
	private String ifsid = null;
	
	@Column(name = "COUNTRY", unique = false, nullable = true)
	private String country = null;
	
	@Column(name = "COMPANY_STATUS", unique = false, nullable = true)
	private String companyStatus = null; 
	
	@Column(name = "ACTIVE_APPLICATIONS", unique = false, nullable = true)
	private String activeApplications;
	
	@Column(name = "SUSPENDED_APPLICATIONS", unique = false, nullable = true)
	private String suspendedApplications; 
	
	@Column(name = "COMPANY_TYPE", unique = false, nullable = true)
	private String companyType;
	
	@Column(name = "ICAO_CODE", unique = false, nullable = true)
	private String icaoCode = null;
	
	@Column(name = "IATA_CODE", unique = false, nullable = true)
	private String iataCode = null;
	
	@Column(name = "AGENCY", unique = false, nullable = true)
	private String agency = null;
	
	@Column(name = "COUNTRY_OF_INC", unique = false, nullable = true)
	private String countryOfIncorporation = null;
	
	@Column(name = "SURROGATE_EMAIL", unique = false, nullable = true)
	private String surrogateEmail = null;
		
}
