package com.mock.service;

import com.exostar.icert.cam.company.companycreateupdaterequesttype.CompanyCreateUpdateRequestType;
import com.exostar.icert.cam.company.companydeleterequesttype.CompanyDeleteRequestType;
import com.exostar.icert.cam.user.getuserauthorizationsrequesttype.GetUserAuthorizationsRequestType;
import com.exostar.icert.cam.user.getuserauthorizationsresponsetype.GetUserAuthorizationsResponseType;
import com.exostar.icert.cam.user.usercreateupdaterequesttype.UserCreateUpdateRequestType;
import com.exostar.icert.cam.user.userdeleterequesttype.UserDeleteRequestType;

public interface CamService {

	public GetUserAuthorizationsResponseType getAllAuthorizationsOfServiceProviderForUser(GetUserAuthorizationsRequestType getUserAuthorizationsRequestType);

	public Integer createCompany(CompanyCreateUpdateRequestType companyCreateUpdateRequestType);

	public Boolean updateCompany(CompanyCreateUpdateRequestType companyCreateUpdateRequestType);

	public Integer createUser(UserCreateUpdateRequestType userCreateUpdateRequestType);

	public Boolean updateUser(UserCreateUpdateRequestType userCreateUpdateRequestType);

	public Boolean deleteUser(UserDeleteRequestType userDeleteRequestType);

	public Boolean deleteCompany(CompanyDeleteRequestType companyDeleteRequestType);
}
