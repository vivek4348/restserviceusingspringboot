package com.mock.service.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exostar.icert.cam.common.AccessRightType;
import com.exostar.icert.cam.common.AuthorizationType;
import com.exostar.icert.cam.common.MessageHeaderType;
import com.exostar.icert.cam.common.ResponseType;
import com.exostar.icert.cam.common.ServiceType;
import com.exostar.icert.cam.company.companycreateupdaterequesttype.CompanyCreateUpdateRequestType;
import com.exostar.icert.cam.company.companydeleterequesttype.CompanyDeleteRequestType;
import com.exostar.icert.cam.user.getuserauthorizationsrequesttype.GetUserAuthorizationsRequestType;
import com.exostar.icert.cam.user.getuserauthorizationsresponsetype.GetUserAuthorizationsResponseType;
import com.exostar.icert.cam.user.usercreateupdaterequesttype.UserCreateUpdateRequestType;
import com.exostar.icert.cam.user.userdeleterequesttype.UserDeleteRequestType;
import com.mock.dao.CamDao;
import com.mock.facade.CamFacadeservice;
import com.mock.service.CamService;


@Service
public class CamServiceImpl implements CamService {
	
	private static final Logger logger = LoggerFactory.getLogger(CamFacadeservice.class);
	
	 @Autowired
	 private CamDao camDao;
	
	public GetUserAuthorizationsResponseType getAllAuthorizationsOfServiceProviderForUser(GetUserAuthorizationsRequestType getUserAuthorizationsRequestType) {
		logger.debug("Comming inside getAllAuthorizationsOfServiceProviderForUser With User: " + getUserAuthorizationsRequestType.getBody().getIfsUserId() + " And SP: " + getUserAuthorizationsRequestType.getBody().getServiceProviderId());
		GetUserAuthorizationsResponseType getUserAuthorizationsResponseType = new GetUserAuthorizationsResponseType();
		
		MessageHeaderType messageHeaderType = new MessageHeaderType();
		MessageHeaderType.MessageHeader messageHeader = new MessageHeaderType.MessageHeader();
		
		messageHeader.setActivity("messageHeaderActivity");
		
		MessageHeaderType.MessageHeader.CloseSequence closeSequence = new MessageHeaderType.MessageHeader.CloseSequence();
		closeSequence.setIdentifier("closeSequence.setIdentifier");
		closeSequence.setLastMsgNumber(BigInteger.valueOf(1001));
		messageHeader.setCloseSequence(closeSequence);
		
		messageHeader.setFrom("CAM@from.com");
		messageHeader.setMessageId("messageHeader.setMessageId");
		
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(new Date());
		XMLGregorianCalendar date2 = null;
		try {
			date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		messageHeader.setMessageTimestamp(date2);
		
		messageHeader.setRefToMessageId("messageHeader.setRefToMessageId");
		messageHeader.setReturnAddress("IFS@return.com");
		
		MessageHeaderType.MessageHeader.Sequence sequence = new MessageHeaderType.MessageHeader.Sequence();
		sequence.setAckRequested("sequence.setAckRequested");
		sequence.setIdentifier("sequence.setIdentifier");
		sequence.setMessageNumber(BigInteger.valueOf(1001));
		messageHeader.setSequence(sequence);
		
		MessageHeaderType.MessageHeader.SequenceAcknowledgement sequenceAcknowledgement = new MessageHeaderType.MessageHeader.SequenceAcknowledgement();
		sequenceAcknowledgement.setIdentifier("sequenceAcknowledgement.setIdentifie");
		messageHeader.setSequenceAcknowledgement(sequenceAcknowledgement);
		
		messageHeader.setTo("IFS@to.com");
		messageHeader.setTransactionName("messageHeader.setTransactionName");
		messageHeader.setVersion("messageHeader.setVersion");
	
		
		messageHeaderType.setMessageHeader(messageHeader);
		
		getUserAuthorizationsResponseType.setHeader(messageHeaderType);
		
		GetUserAuthorizationsResponseType.Body body = new GetUserAuthorizationsResponseType.Body();
		body.setEndpointId("body.setEndpointId");
		body.setIFSUserId("body.setIFSUserId");
		body.setServiceCollectionId("body.setServiceCollectionId");
		body.setServiceId("body.setServiceId");
		body.setServiceProviderId("body.setServiceProviderId");
		AuthorizationType authorizationType = new AuthorizationType();
		authorizationType.setAuthorizationId("authorizationType.setAuthorizationId");
		authorizationType.setDateValidFrom(date2);
		authorizationType.setDateValidTo(date2);
		ServiceType serviceType = new ServiceType();
		
		AccessRightType accessRightType = new AccessRightType();
		accessRightType.setClaim(this.camDao.getAllAuthorizationsOfServiceProviderForUser(getUserAuthorizationsRequestType.getBody().getIfsUserId(),
				getUserAuthorizationsRequestType.getBody().getServiceProviderId()));
		
		serviceType.setCompanyId("setCompanyId");
		serviceType.setCreatedUser("setCreatedUser");
		serviceType.setDateCreatedOn(date2);		
		serviceType.setDateUpdatedOn(date2);		
		serviceType.setDescription("setDescription");
		serviceType.setIsActive(true);
		serviceType.setIsDeleted(true);
		serviceType.setName("ServiceTypeName");		
		serviceType.setServiceId("setServiceId");		
		serviceType.setUpdatedUser("setUpdatedUser");
		
		serviceType.addAccessRightType(accessRightType);
		authorizationType.addAuthService(serviceType);
		body.addAuthorization(authorizationType);
		getUserAuthorizationsResponseType.setBody(body);
		
		ResponseType responseType = new ResponseType();	
		ResponseType.Body rsBody = new ResponseType.Body(); 
		responseType.setBody(rsBody);
		//MessageHeaderType messageHeaderType = new MessageHeaderType();
		responseType.setHeader(messageHeaderType);	
		responseType.setReturnCode("SuccessCode");
		getUserAuthorizationsResponseType.setResponse(responseType);
		
		return getUserAuthorizationsResponseType;		
	}

	@Override
	public Integer createCompany(CompanyCreateUpdateRequestType companyCreateUpdateRequestType) {
		return null;
	}

	@Override
	public Boolean updateCompany(CompanyCreateUpdateRequestType companyCreateUpdateRequestType) {
		return null;
	}

	@Override
	public Integer createUser(UserCreateUpdateRequestType userCreateUpdateRequestType) {
		return null;
	}

	@Override
	public Boolean updateUser(UserCreateUpdateRequestType userCreateUpdateRequestType) {
		return null;
	}

	@Override
	public Boolean deleteUser(UserDeleteRequestType userDeleteRequestType) {
		return null;
	}

	@Override
	public Boolean deleteCompany(CompanyDeleteRequestType companyDeleteRequestType) {
		return null;
	}

}